package server

import (
	"WBL0/internal/app/storage"
	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
)

// Start listening and serving ...
func Start(ctx context.Context, s *http.Server) error {

	ctx, cancel := context.WithCancel(ctx)
	go handleSignals(cancel)

	go func() {
		<-ctx.Done()
		log.Println("Server shutdown")
		s.Shutdown(context.TODO())
	}()
	log.Println(s.Addr)
	return s.ListenAndServe()
}

// New - configure new server
func New() *http.Server {

	gin.SetMode(gin.ReleaseMode)

	router := gin.Default()
	tmpl, err := template.ParseGlob("templates/*")
	if err != nil {
		log.Println(err)
	}
	router.SetHTMLTemplate(tmpl)
	router.GET("/", homeHandler)
	router.POST("/data", infoHandler)

	addr := fmt.Sprintf("%s%s", os.Getenv("SERVICE_HOST"), os.Getenv("SERVICE_PORT"))

	return &http.Server{
		Addr:           addr,
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		IdleTimeout:    30 * time.Second,
		MaxHeaderBytes: 1 << 31,
	}
}

func handleSignals(cancel context.CancelFunc) {
	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM, syscall.SIGQUIT)

	log.Printf("Caught signal %s\n", <-sigChan)
	cancel()
	return
}

func homeHandler(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "home.html", nil)
}

func infoHandler(ctx *gin.Context) {
	form := ctx.PostForm("id")
	cache := storage.NewCache()
	v, ok := cache.Orders[form]

	if !ok {
		description := fmt.Sprintf("Order with uid: '%s' not exist", form)
		ctx.JSON(http.StatusNotFound, gin.H{
			"Status":      http.StatusNotFound,
			"Description": description,
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"Data":   v,
		"Status": http.StatusOK,
	})
}
