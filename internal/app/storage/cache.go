package storage

import (
	model "WBL0/internal/models"
	"context"
	"encoding/json"
	"log"
	"sync"
)

type Cache struct {
	Orders map[string]model.Order
	mutex  sync.Mutex
}

var cacheInstance *Cache

type collisionError struct{}

func (c *collisionError) Error() string {
	return "Order already exist"
}

func (c *Cache) Write(ctx context.Context, data []byte, uid string) error {
	order := new(model.Order)
	err := json.Unmarshal(data, order)
	if err != nil {
		return err
	}

	if _, ok := c.Orders[uid]; ok {
		return new(collisionError)
	}

	c.mutex.Lock()
	c.Orders[uid] = *order
	c.mutex.Unlock()
	return nil
}

func (c *Cache) Get(uid string) model.Order {
	return c.Orders[uid]
}

func NewCache() *Cache {
	if cacheInstance != nil {
		return cacheInstance
	}

	pg := NewPg()
	ctx := context.Background()

	hash, err := pg.GetAll(ctx)
	if err != nil {
		log.Println(err)
	}
	return &Cache{Orders: hash}
}
